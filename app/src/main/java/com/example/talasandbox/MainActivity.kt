package com.example.talasandbox

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.talasandbox.common.Currency
import com.example.talasandbox.common.ISO_CODE_MX_CENTAVO
import com.example.talasandbox.common.Money
import com.example.talasandbox.common.PaymentDetails
import com.example.talasandbox.databinding.ActivityMain1Binding
import com.example.talasandbox.mvp.PaymentDetailsViewMxOxxo
import com.example.talasandbox.mvvm.MainViewModel

const val USE_MVP = false

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (USE_MVP) {
            startUpAsMVP()
        } else {
            startUpAsMvvm()
        }
    }


    private fun startUpAsMVP() {
        val paymentDetails =
            PaymentDetails.Mx.Oxxo(
                Money.fromMajorUnit(1000.0, Currency.fromIsoCode(ISO_CODE_MX_CENTAVO)),
                Money.fromMajorUnit(5.0, Currency.fromIsoCode(ISO_CODE_MX_CENTAVO)),
                "Joe User",
                "42"
            )
        setContentView(R.layout.activity_main)
        findViewById<ConstraintLayout>(R.id.main_layout)?.addView(
            PaymentDetailsViewMxOxxo(this@MainActivity, paymentDetails)
        )
    }


    private lateinit var vm: MainViewModel

    private fun startUpAsMvvm() {
        vm = ViewModelProvider(this)[MainViewModel::class.java]
        val binding: ActivityMain1Binding = DataBindingUtil.setContentView(this, R.layout.activity_main_1)
        binding.lifecycleOwner = this
        binding.vm = vm
    }

    override fun onStart() {
        super.onStart()
        if (!USE_MVP) {
            vm.setPaymentDetails(this)
        }
    }
}
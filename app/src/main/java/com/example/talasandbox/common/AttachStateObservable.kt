package com.example.talasandbox

import android.view.View
import com.example.talasandbox.common.RxPresenter

abstract class WidgetRxPresenter<V : AttachStateObservable>(view: V) :
    RxPresenter(),
    View.OnAttachStateChangeListener {

    init {
        view.addOnAttachStateChangeListener(this)
    }

    override fun onViewAttachedToWindow(view: View) {
        // Nothing to do on attach. Disposables aren't guaranteed to be created when the view is. Implementation
        // is responsible for creating them when necessary (i.e. might be created on button click or pull to refresh)
    }

    override fun onViewDetachedFromWindow(view: View) {
        onStop()
    }
}

interface AttachStateObservable {
    fun addOnAttachStateChangeListener(listener: View.OnAttachStateChangeListener)
}
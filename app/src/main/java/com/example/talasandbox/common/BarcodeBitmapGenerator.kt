package com.example.talasandbox.common

import android.graphics.Bitmap
import io.reactivex.Single

interface BarcodeBitmapGenerator  {

    /**
     * Function to generate bitmap barcode, given the data in the barcodeData object
     */
    fun generateBitmap(barcodeData: BarcodeData): Single<Bitmap>
}

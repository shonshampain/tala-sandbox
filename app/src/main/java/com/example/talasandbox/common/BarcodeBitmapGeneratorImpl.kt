package com.example.talasandbox.common

import android.graphics.Bitmap
import com.google.zxing.BarcodeFormat
import com.google.zxing.oned.Code128Writer
import io.reactivex.Single
import javax.inject.Inject

class BarcodeBitmapGeneratorImpl @Inject constructor() : BarcodeBitmapGenerator  {
    override fun generateBitmap(barcodeData: BarcodeData): Single<Bitmap> {
        return Single.defer {
            Single.just(generateBarcode(barcodeData))
        }
    }

    private fun generateBarcode(barcodeData: BarcodeData): Bitmap {
        val bitMatrix = Code128Writer().encode(
            barcodeData.text,
            BarcodeFormat.CODE_128,
            barcodeData.widthPixels,
            barcodeData.heightPixels
        )
        val bitmap = Bitmap.createBitmap(bitMatrix.width, bitMatrix.height, Bitmap.Config.ARGB_8888)
        for (y in 0 until bitMatrix.height) {
            for (x in 0 until bitMatrix.width) {
                val pixelColor = if (bitMatrix.get(x, y)) {
                    barcodeData.barcodeColor
                } else {
                    barcodeData.backgroundColor
                }
                bitmap.setPixel(x, y, pixelColor)
            }
        }
        return bitmap
    }
}

package com.example.talasandbox.common

import java.io.Serializable

data class BarcodeData(
    val text: String,
    val barcodeColor: Int,
    val backgroundColor: Int,
    val widthPixels: Int,
    val heightPixels: Int
) : Serializable

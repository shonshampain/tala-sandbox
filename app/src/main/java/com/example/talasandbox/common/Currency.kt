package com.example.talasandbox.common

import androidx.annotation.VisibleForTesting
import java.io.Serializable

private const val MULTIPLIER_MX = 100
private const val MULTIPLIER_PH = 100
private const val MULTIPLIER_KE = 100
private const val MULTIPLIER_IN = 1
private const val MULTIPLIER_US = 100

const val ISO_CODE_MX_PESO = "MXN"
const val ISO_CODE_MX_CENTAVO = "MXn"
const val ISO_CODE_PH = "PHP"
const val ISO_CODE_KE = "KES"
const val ISO_CODE_IN = "INR"
const val ISO_CODE_US = "USD"

// TODO Class names should be addresses in [UW-1885] aageychenko 05 Mar 2021
@Suppress("ClassNaming")
object CURRENCY_MX : Currency(MULTIPLIER_MX, ISO_CODE_MX_CENTAVO) {
    private fun readResolve(): Any = CURRENCY_MX
}
@Suppress("ClassNaming")
object CURRENCY_PH : Currency(MULTIPLIER_PH, ISO_CODE_PH) {
    private fun readResolve(): Any = CURRENCY_PH
}
@Suppress("ClassNaming")
object CURRENCY_KE : Currency(MULTIPLIER_KE, ISO_CODE_KE) {
    private fun readResolve(): Any = CURRENCY_KE
}
@Suppress("ClassNaming")
object CURRENCY_IN : Currency(MULTIPLIER_IN, ISO_CODE_IN) {
    private fun readResolve(): Any = CURRENCY_IN
}
@Suppress("ClassNaming")
object CURRENCY_US : Currency(MULTIPLIER_US, ISO_CODE_US) {
    private fun readResolve(): Any = CURRENCY_US
}

sealed class Currency @VisibleForTesting constructor(
    /**
     * Represents amount of minor currency units from which 1 major unit consists of.
     * Example: 1 dollar consists of 100 cents, 100 is a multiplier.
     */
    val multiplier: Int,

    /**
     * ISO 4217, currency code with cases-sensitive ending denoting denomination.
     * The currency code denotes the currency system, not the particular denomination (e.g. dollar/cent)
     * Example: USD for dollars, USd for cents
     */
    val isoCode: String
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (other == null) {
            return false
        }
        if (other !is Currency) {
            return false
        }

        return this.multiplier == other.multiplier &&
                this.isoCode == other.isoCode
    }

    override fun hashCode(): Int {
        return multiplier.hashCode() xor isoCode.hashCode()
    }

    companion object {
        /**
         * Convenience function to build [Currency] with isoCode.
         * !!! Only the default minimally recognized denomination should be used across the app.
         * @param isoCode can be both major or minor units (e.g MXN for Peso and MXn for centavo), e.g.
         *                - USD corresponds to Dollars with multiplier 1,
         *                - USd corresponds to cents with multiplier 100.
         * @return data with default canonical uppercase ISO code.
         */
        fun fromIsoCode(isoCode: String): Currency {
            return when (isoCode) {
                ISO_CODE_MX_CENTAVO -> CURRENCY_MX
                ISO_CODE_PH -> CURRENCY_PH
                ISO_CODE_KE -> CURRENCY_KE
                ISO_CODE_IN -> CURRENCY_IN
                ISO_CODE_US -> CURRENCY_US
                else -> throw IllegalArgumentException("$isoCode is not supported")
            }
        }
    }
}
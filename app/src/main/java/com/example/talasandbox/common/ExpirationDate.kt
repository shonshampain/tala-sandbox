package com.example.talasandbox.common

import java.io.Serializable
import java.util.*

sealed class ExpirationDate : Serializable {

    abstract fun hasExpired(): Boolean

    /**
     * Expiration date that cannot expire. Unlimited time of life.
     */
    class Unlimited : ExpirationDate() {

        override fun hasExpired() = false
    }

    /**
     * Expiration date that can expire. Limited in time, will expire at [expiryDate].
     * @param expiryDate date without timezone (in UTC)
     */
    class Limited(val expiryDate: Date) : ExpirationDate() {

        override fun hasExpired() = Date().after(expiryDate)
    }
}

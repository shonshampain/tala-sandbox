package com.example.talasandbox.common

import androidx.annotation.VisibleForTesting
import java.io.Serializable
import kotlin.math.abs
import kotlin.math.round
import kotlin.math.roundToLong

data class Money private constructor(
    val minorUnitAmount: Long,
    val currency: Currency
) : Serializable, Comparable<Money> {

    companion object {
        /**
         * Builds an instance of [Money] assuming the passed amount value
         * is represented in major currency units.
         * Note: If major currency unit has more decimals digits than acceptable,
         * [IllegalArgumentException] is thrown. The acceptable decimal units are
         * obtained from [Currency.multiplier].
         */
        fun fromMajorUnit(majorUnitAmount: Double, currency: Currency): Money {
            try {
                val minorUnitAmountDouble = majorUnitAmount * currency.multiplier

                // minor unit amount is not expected to have any decimals
                require(minorUnitAmountDouble.isNearlyInteger()) {
                    "$majorUnitAmount cannot be converted to integer minor units amount"
                }

                return Money(majorUnitAmount.fromMajorToMinor(currency), currency)
            } catch (exc: IllegalStateException) {
                throw IllegalArgumentException("Cannot create money amount", exc)
            }
        }

        /**
         * Builds an instance of [Money] assuming the passed amount value
         * is represented in minor currency units.
         */
        fun fromMinorUnit(minorUnitAmount: Long, currency: Currency): Money {
            return Money(minorUnitAmount, currency)
        }

        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        fun Double.fromMajorToMinor(currency: Currency): Long {
            return (this * currency.multiplier).roundToLongChecked()
        }

        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        fun Long.fromMinorToMajor(currency: Currency): Double {
            return this.toDouble() / currency.multiplier
        }

        /**
         * Same as [Double.toLong()] but handles positive Long overflow.
         * @throws IllegalStateException in case of overflow
         */
        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        fun Double.roundToLongChecked(): Long {
            require(this <= Long.MAX_VALUE) { "$this overflows Long" }
            require(this >= Long.MIN_VALUE) { "$this overflows Long" }
            return this.roundToLong()
        }
    }

    /**
     * @return [minorUnitAmount] in a major unit representation.
     * Example: 5.5 for 550 amount with 100 multiplier
     */
    val majorUnitAmount: Double get() = minorUnitAmount.fromMinorToMajor(currency)

    /**
     *  Returns zero if this [Money.minorUnitAmount] is equal to the specified other
     *  [Money.minorUnitAmount], a negative number if it's less than other, or a positive number
     *  if it's greater than other.
     * @throws IllegalStateException if argument's [Money.currency] is different.
     */
    override fun compareTo(other: Money): Int {
        require(currency == other.currency) { "Cannot compare different currencies ($currency, ${other.currency})" }

        return minorUnitAmount.compareTo(other.minorUnitAmount)
    }

    /**
     * Summarize the amount of two [Money].
     * @throws IllegalStateException if argument's [Money.currency] is different.
     */
    operator fun plus(other: Money): Money {
        require(currency == other.currency) { "Cannot add different currencies ($currency, ${other.currency})" }

        return fromMinorUnit(minorUnitAmount + other.minorUnitAmount, currency)
    }

    /**
     * Subtract the provided [Money] from the current one.
     * @throws IllegalStateException if argument's [Money.currency] is different.
     */
    operator fun minus(other: Money): Money {
        require(currency == other.currency) { "Cannot subtract different currencies ($currency, ${other.currency})" }
        return fromMinorUnit(minorUnitAmount - other.minorUnitAmount, currency)
    }

    /**
     * Multiply this [Money] by given argument.
     * The fractional part, if any, is rounded down towards zero as per [Double.toLong]
     */
    operator fun times(multiplier: Double): Money {
        val multipliedAmount = minorUnitAmount * multiplier
        return fromMinorUnit(multipliedAmount.roundToLong(), currency)
    }
}

/**
 * Returns true if the money amount is greater than or equal to 0. Used for formatting & coloring text.
 */
fun Money.isPositiveOrZero(): Boolean = minorUnitAmount >= 0
private const val DOUBLE_EQUALITY_THRESHOLD = 0.000001

fun Double.nearlyEquals(targetDouble: Double, epsilon: Double = DOUBLE_EQUALITY_THRESHOLD): Boolean {
    if (targetDouble.compareTo(this) == 0) {
        return true
    }
    val diff = abs(this - targetDouble)
    return diff <= epsilon
}

fun Double.isNearlyInteger(): Boolean {
    val rounded = round(this)
    return this.nearlyEquals(rounded)
}
package com.example.talasandbox.common

import java.io.Serializable

/**
 * Payment details for [co.tala.common.payments.domain.model.PaymentChannelType]
 */
@Suppress("LongParameterList")
sealed class PaymentDetails : Serializable {
    /**
     * Amount due without fee amount included.
     */
    // TODO use money amount class instead of doubles. dima 07/14/2020
    abstract val amountDueWithoutFee: Money

    /**
     * Fee amount.
     */
    // TODO use money amount class instead of doubles. dima 07/14/2020
    abstract val feeAmount: Money

    sealed class Ke : PaymentDetails() {

        class InApp(
            override val amountDueWithoutFee: Money,
            override val feeAmount: Money
        ) : Ke()

        data class Paybill(
            override val amountDueWithoutFee: Money,
            override val feeAmount: Money,
            val paybill: String,
            val accountNumber: String
        ) : Ke()
    }

    sealed class Ph : PaymentDetails() {

        data class Coins(
            override val amountDueWithoutFee: Money,
            override val feeAmount: Money,
            val title: String,
            val description: String
        ) : Ph()

        data class CebuanaLhuiller(
            override val amountDueWithoutFee: Money,
            override val feeAmount: Money,
            val controlNumber: String,
            val form: String,
            val transactionType: String,
            val sender: String,
            val companyName: String
        ) : Ph()

        data class MLhuiller(
            override val amountDueWithoutFee: Money,
            override val feeAmount: Money,
            val title: String,
            val description: String
        ) : Ph()

        sealed class GCash : Ph() {
            /**
             * Reference code that should be used for payment in store which supports [GCash].
             */
            abstract val referenceNumber: String

            /**
             * Url of the barcode that contains [referenceNumber], can be used  in store which supports [GCash].
             */
            abstract val barCodeUrl: String?

            /**
             * Date when [referenceNumber] may expire.
             */
            abstract val expiryDate: ExpirationDate

            /**
             * Place that supports [GCash] payments.
             */
            data class SevenEleven(
                override val amountDueWithoutFee: Money,
                override val feeAmount: Money,
                override val referenceNumber: String,
                override val barCodeUrl: String?,
                override val expiryDate: ExpirationDate
            ) : GCash()
        }

        sealed class PayMaya : Ph() {
            /**
             * Reference code that should be used for payment in store which supports [GCash].
             */
            abstract val referenceNumber: String

            /**
             * Url of the barcode that contains [referenceNumber], can be used  in store which supports [GCash].
             */
            abstract val barCodeUrl: String?

            /**
             * Date when [referenceNumber] may expire.
             */
            abstract val expiryDate: ExpirationDate

            /**
             * Place that supports [PayMaya] payments.
             */
            data class SevenEleven(
                override val amountDueWithoutFee: Money,
                override val feeAmount: Money,
                override val referenceNumber: String,
                override val barCodeUrl: String?,
                override val expiryDate: ExpirationDate,
            ) : PayMaya()
        }

        /**
         * Payment channels that are going via ECPay provider.
         */
        sealed class EcPay : Ph() {
            /**
             * Reference code that should be used for payment in store which supports ECPay.
             */
            abstract val referenceNumber: String
            /**
             * Url of the barcode that contains [referenceNumber], can be used  in store which supports ECPay.
             */
            abstract val barCodeUrl: String
            /**
             * Date when [referenceNumber] may expire.
             */
            abstract val expiryDate: ExpirationDate

            /**
             * Place that supports ECPay payments.
             */
            data class SevenEleven(
                override val amountDueWithoutFee: Money,
                override val feeAmount: Money,
                override val referenceNumber: String,
                override val barCodeUrl: String,
                override val expiryDate: ExpirationDate
            ) : EcPay()
        }
        /**
         * Payment channels that are going via Cliqq provider.
         */
        sealed class Cliqq : Ph() {
            /**
             * Reference code that should be used for payment in store which supports Cliqq.
             */
            abstract val referenceNumber: String
            /**
             * Url of the barcode that contains [referenceNumber], can be used  in store which supports Cliqq.
             */
            abstract val barCodeUrl: String?
            /**
             * Date when [referenceNumber] may expire.
             */
            abstract val expiryDate: ExpirationDate

            /**
             * Place that supports Cliqq payments.
             */
            data class SevenEleven(
                override val amountDueWithoutFee: Money,
                override val feeAmount: Money,
                override val referenceNumber: String,
                override val barCodeUrl: String?,
                override val expiryDate: ExpirationDate
            ) : Cliqq()
        }
    }

    sealed class Mx : PaymentDetails() {

        class Oxxo(
            override val amountDueWithoutFee: Money,
            override val feeAmount: Money,
            val userName: String,
            val controlNumber: String
        ) : Mx()

        class Bank(
            override val amountDueWithoutFee: Money,
            // bank doesn't have fees, so zero is used by default
            override val feeAmount: Money = Money.fromMajorUnit(0.0, CURRENCY_MX),
            val clabeNumber: String,
            val paymentComment: String,
            val providerDetails: String,
            val accountTypeDetails: String,
            val bankDetails: String
        ) : Mx()

        class Transfer(
            override val amountDueWithoutFee: Money,
            override val feeAmount: Money,
            val clabeNumber: String
        ) : Mx()

        class BankTransfer(
            override val amountDueWithoutFee: Money,
            // bank transfer doesn't have fees, so zero is used by default
            override val feeAmount: Money = Money.fromMajorUnit(0.0, CURRENCY_MX),
            val clabeNumber: String,
            val paymentComment: String,
            val providerDetails: String,
            val accountTypeDetails: String,
            val bankDetails: String
        ) : Mx()

        sealed class Paynet : Mx() {
            abstract val referenceNumber: String

            data class GenericStore(
                override val amountDueWithoutFee: Money,
                override val feeAmount: Money,
                override val referenceNumber: String
            ) : Paynet()

            data class SevenEleven(
                override val amountDueWithoutFee: Money,
                override val feeAmount: Money,
                override val referenceNumber: String
            ) : Paynet()

            data class FarmaciasDelAhorro(
                override val amountDueWithoutFee: Money,
                override val feeAmount: Money,
                override val referenceNumber: String
            ) : Paynet()

            data class BodegaAurrera(
                override val amountDueWithoutFee: Money,
                override val feeAmount: Money,
                override val referenceNumber: String
            ) : Paynet()
        }
    }
}
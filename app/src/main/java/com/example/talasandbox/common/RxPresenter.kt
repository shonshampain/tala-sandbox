package com.example.talasandbox.common

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class RxPresenter {

    private val compositeDisposable = CompositeDisposable()

    protected fun onStop() {
        compositeDisposable.clear()
    }

    protected fun Disposable.trackDisposable() = compositeDisposable.add(this)
}

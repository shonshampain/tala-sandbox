package com.example.talasandbox.mvp

interface MvpContract {

    interface View

    interface Presenter<T : View> {
        var view: T
    }
}
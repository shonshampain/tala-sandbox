package com.example.talasandbox.mvp

import android.graphics.Bitmap
import com.example.talasandbox.AttachStateObservable
import com.example.talasandbox.common.BarcodeData

interface PaymentDetailsMxOxxoContract {

    interface View : MvpContract.View, AttachStateObservable {
        fun setBarcodeVisibility(isVisible: Boolean)
        fun displayBarcodeImage(image: Bitmap)
    }

    interface Presenter : MvpContract.Presenter<View> {
        fun onStartWithData(barcodeData: BarcodeData)
    }
}

package com.example.talasandbox.mvp

import androidx.core.graphics.translationMatrix
import com.example.talasandbox.WidgetRxPresenter
import com.example.talasandbox.common.BarcodeBitmapGenerator
import com.example.talasandbox.common.BarcodeData
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.computation
import javax.inject.Inject

class PaymentDetailsMxOxxoPresenter @Inject constructor(
    override var view: PaymentDetailsMxOxxoContract.View,
    private val barcodeBitmapGenerator: BarcodeBitmapGenerator,
) : WidgetRxPresenter<PaymentDetailsMxOxxoContract.View>(view),
    PaymentDetailsMxOxxoContract.Presenter {
    override fun onStartWithData(barcodeData: BarcodeData){
        barcodeBitmapGenerator.generateBitmap(barcodeData)
            .subscribeOn(computation())
            .observeOn(mainThread())
            .subscribe({
                view.displayBarcodeImage(it)
                view.setBarcodeVisibility(true)
            }, {
                view.setBarcodeVisibility(false)
            })
            .trackDisposable()
    }
}

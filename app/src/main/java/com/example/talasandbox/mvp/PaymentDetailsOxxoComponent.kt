package com.example.talasandbox.mvp

import com.example.talasandbox.common.ActivityScope
import dagger.BindsInstance
import dagger.Component

@ActivityScope
@Component(modules = [PaymentDetailsOxxoModule::class])
interface PaymentDetailsOxxoComponent {
    fun inject(view: PaymentDetailsViewMxOxxo)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance view: PaymentDetailsMxOxxoContract.View,
        ): PaymentDetailsOxxoComponent
    }

    companion object {
        fun get(view: PaymentDetailsViewMxOxxo): PaymentDetailsOxxoComponent {
            return DaggerPaymentDetailsOxxoComponent.factory().create(view)
        }
    }
}

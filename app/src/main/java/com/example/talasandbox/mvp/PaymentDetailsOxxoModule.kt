package com.example.talasandbox.mvp

import com.example.talasandbox.common.ActivityScope
import com.example.talasandbox.common.BarcodeBitmapGenerator
import com.example.talasandbox.common.BarcodeBitmapGeneratorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class PaymentDetailsOxxoModule {

    @Binds
    @ActivityScope
    abstract fun bindBarcodeBitmapGenerator(
        barcodeBitmapGenerator: BarcodeBitmapGeneratorImpl
    ): BarcodeBitmapGenerator

    @ActivityScope
    @Binds
    abstract fun bindPresenter(presenter: PaymentDetailsMxOxxoPresenter): PaymentDetailsMxOxxoContract.Presenter
}

package com.example.talasandbox.mvp

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.talasandbox.R
import com.example.talasandbox.common.BarcodeData
import com.example.talasandbox.common.PaymentDetails
import kotlinx.android.synthetic.main.payment_details_view_mx_oxxo.view.*
import javax.inject.Inject

fun View.setVisibleOrGone(isVisible: Boolean) {
    this.visibility = if (isVisible) View.VISIBLE else View.GONE
}
/**
 * View that renders payment details for [PaymentDetails.Mx.Oxxo].
 *
 * This constructor should always have these parameters because it's invoked from code,
 * it should not be used from xml layout.
 */
@SuppressLint("ViewConstructor")
class PaymentDetailsViewMxOxxo(
    context: Context,
    paymentDetails: PaymentDetails.Mx.Oxxo
) : PaymentDetailsMxOxxoContract.View, ConstraintLayout(context, null) {

    @Inject
    lateinit var presenter: PaymentDetailsMxOxxoContract.Presenter

    init {
        LayoutInflater.from(context).inflate(R.layout.payment_details_view_mx_oxxo, this)
        injectDependencies()
        setPaymentDetails(paymentDetails.controlNumber)
    }

    private fun setPaymentDetails(controlNumber: String) {
        control_number_text.text = controlNumber

        val heightPixels = resources.getDimensionPixelSize(R.dimen.payment_barcode_height)
        val widthPixels = resources.getDimensionPixelSize(R.dimen.payment_barcode_width)

        val barcodeData = BarcodeData(
            text = controlNumber,
            barcodeColor = ContextCompat.getColor(context, R.color.black),
            backgroundColor = ContextCompat.getColor(context, android.R.color.white),
            widthPixels = widthPixels,
            heightPixels = heightPixels,
        )
        presenter.onStartWithData(barcodeData)
    }

    private fun injectDependencies() {
        PaymentDetailsOxxoComponent.get(this).inject(this)
    }

    override fun setBarcodeVisibility(isVisible: Boolean) {
        payment_bar_code_image.setVisibleOrGone(isVisible)
    }

    override fun displayBarcodeImage(image: Bitmap) {
        payment_bar_code_image.setImageBitmap(image)
    }
}
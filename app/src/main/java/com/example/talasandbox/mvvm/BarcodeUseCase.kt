package com.example.talasandbox.mvvm

import android.graphics.Bitmap
import com.example.talasandbox.common.BarcodeBitmapGenerator
import com.example.talasandbox.common.BarcodeData
import io.reactivex.Single
import javax.inject.Inject

interface BarcodeUseCase {
    fun generateBarcode(barcodeData: BarcodeData): Single<Bitmap>
}

class BarcodeUseCaseImpl @Inject constructor(
    private val barcodeBitmapGenerator: BarcodeBitmapGenerator
) : BarcodeUseCase {

    override fun generateBarcode(barcodeData: BarcodeData): Single<Bitmap> =
        barcodeBitmapGenerator.generateBitmap(barcodeData)
}
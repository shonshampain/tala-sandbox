package com.example.talasandbox.mvvm

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.talasandbox.R
import com.example.talasandbox.common.*
import com.example.talasandbox.mvvm.di.AppComponent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel : ViewModel() {

    @Inject
    lateinit var barcodeUseCase: BarcodeUseCase

    private val paymentDetails =
        PaymentDetails.Mx.Oxxo(
            Money.fromMajorUnit(1000.0, Currency.fromIsoCode(ISO_CODE_MX_CENTAVO)),
            Money.fromMajorUnit(5.0, Currency.fromIsoCode(ISO_CODE_MX_CENTAVO)),
            "Joe User",
            "42"
        )

    private val _barcodeIsVisible = MutableLiveData<Boolean>().apply { value = true }
    val barcodeIsVisible: LiveData<Boolean> = _barcodeIsVisible
    private val _barcode = MutableLiveData<BitmapDrawable?>().apply{ value = null }
    val barcode: LiveData<BitmapDrawable?> = _barcode
    private val _controlNumberText = MutableLiveData<String>().apply { value = "xxx" }
    val controlNumberText: LiveData<String> = _controlNumberText

    private val cd = CompositeDisposable()

    init {
        AppComponent.get(this).inject(this)
    }

    fun setPaymentDetails(context: Context) {
        _controlNumberText.value = paymentDetails.controlNumber
        Log.d("MainViewModel", "Setting control number to ${paymentDetails.controlNumber}")

        val barcodeData = BarcodeData(
            text = paymentDetails.controlNumber,
            barcodeColor = ContextCompat.getColor(context, R.color.black),
            backgroundColor = ContextCompat.getColor(context, android.R.color.white),
            widthPixels = context.resources.getDimensionPixelSize(R.dimen.payment_barcode_width),
            heightPixels = context.resources.getDimensionPixelSize(R.dimen.payment_barcode_height),
        )
        viewModelScope.launch {
            cd += barcodeUseCase.generateBarcode(barcodeData)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _barcode.value = BitmapDrawable(context.resources, it)
                    _barcodeIsVisible.value = true
                }, {
                    _barcodeIsVisible.value = false
                })
        }
    }

    override fun onCleared() {
        cd.dispose()
    }
}
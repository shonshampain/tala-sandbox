package com.example.talasandbox.mvvm.di

import com.example.talasandbox.mvvm.DaggerAppComponent
import com.example.talasandbox.mvvm.MainViewModel
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class
    ]
)

interface AppComponent {
    fun inject(vm: MainViewModel)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance vm: MainViewModel,
        ): AppComponent
    }

    companion object {
        fun get(vm: MainViewModel): AppComponent {
            return DaggerAppComponent.factory().create(vm)
        }
    }

}
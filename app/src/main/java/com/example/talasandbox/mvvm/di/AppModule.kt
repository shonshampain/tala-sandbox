package com.example.talasandbox.mvvm.di

import com.example.talasandbox.common.BarcodeBitmapGenerator
import com.example.talasandbox.common.BarcodeBitmapGeneratorImpl
import com.example.talasandbox.mvvm.BarcodeUseCase
import com.example.talasandbox.mvvm.BarcodeUseCaseImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {
    @Binds
    @Singleton
    abstract fun bindBarcodeBitmapGenerator(
        barcodeBitmapGenerator: BarcodeBitmapGeneratorImpl
    ): BarcodeBitmapGenerator

    @Binds
    @Singleton
    abstract fun providesBarcodeUseCase(barcodeUseCaseImpl: BarcodeUseCaseImpl) : BarcodeUseCase
}
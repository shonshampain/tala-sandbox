package com.example.talasandbox.mvp

import android.graphics.Bitmap
import android.graphics.Color
import com.example.talasandbox.common.BarcodeBitmapGenerator
import com.example.talasandbox.common.BarcodeData
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExternalResource
import org.mockito.Answers
import org.mockito.Mockito.verify
import org.mockito.Mockito.never
import org.mockito.stubbing.OngoingStubbing

private const val BAR_CODE_TEXT = "123456789"
private const val BAR_CODE_COLOR = Color.BLUE
private const val BAR_CODE_BACKGROUND_COLOR = Color.YELLOW
private const val BAR_CODE_WIDTH = 400
private const val BAR_CODE_HEIGHT = 100
private const val DUMMY_ERROR_MESSAGE = "error_message"

inline fun <reified T : Any> stubMock(): T = mock(stubOnly = true, defaultAnswer = Answers.RETURNS_SMART_NULLS)
typealias SingleStubbing<T> = OngoingStubbing<Single<T>>
fun <T> SingleStubbing<T>.thenReturnSuccess(value: T): SingleStubbing<T> = thenReturn(io.reactivex.Single.just(value))
fun <T> SingleStubbing<T>.thenReturnError(error: Throwable): SingleStubbing<T> = thenReturn(io.reactivex.Single.error(error))

class RxSchedulersTrampolineTestRule : ExternalResource() {
    private var defaultExceptionHandler: Thread.UncaughtExceptionHandler? = null

    override fun before() {
        // Remember current default exception handler and replace it with one that rethrows the exception
        // The default handler logs the exception and ignores is and RxJava call the exception handler directly
        // not rethrowing it, that causes exception to be ignored by unit-tests, but not in production where
        // in the same situation Android's default handler crashes the app.
        // Rethrowing allows unit test fail in the same place the application would fail.
        defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { _, exc -> throw exc }

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        // Do not set setComputationSchedulerHandler here, it breaks CI for some reason
        // build is stuck on :support:testDebugUnitTest, haven't figured why yet
    }

    override fun after() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()

        Thread.setDefaultUncaughtExceptionHandler(defaultExceptionHandler)
    }
}

interface HasRxTrampolineRule {

    @Rule
    fun rxTrampolineRule() = RxSchedulersTrampolineTestRule()
}

class PaymentDetailsMxOxxoPresenterTest : HasRxTrampolineRule {

    private val view = mock<PaymentDetailsMxOxxoContract.View>()
    private val barcodeBitmapGenerator = mock<BarcodeBitmapGenerator>()
    private val barcodeData = BarcodeData(
        BAR_CODE_TEXT,
        BAR_CODE_COLOR,
        BAR_CODE_BACKGROUND_COLOR,
        BAR_CODE_WIDTH,
        BAR_CODE_HEIGHT
    )

    private val bitmap: Bitmap = stubMock()
    private val presenter: PaymentDetailsMxOxxoPresenter = PaymentDetailsMxOxxoPresenter(view, barcodeBitmapGenerator)

    @Test
    fun `when setBarCode is passed valid arguments then barcode is displayed and set visible`() {
        // GIVEN
        whenever(barcodeBitmapGenerator.generateBitmap(barcodeData)).thenReturnSuccess(bitmap)
        // WHEN
        presenter.onStartWithData(barcodeData)
        // THEN
        verify(view).displayBarcodeImage(bitmap)
        verify(view).setBarcodeVisibility(true)
    }

    @Test
    fun `when setBarCode is passed invalid arguments then barcode is not displayed`() {
        // GIVEN
        whenever(barcodeBitmapGenerator.generateBitmap(barcodeData)).thenReturnError(Exception(DUMMY_ERROR_MESSAGE))
        // WHEN
        presenter.onStartWithData(barcodeData)
        // THEN
        verify(view, never()).displayBarcodeImage(any())
        verify(view).setBarcodeVisibility(false)
    }
}